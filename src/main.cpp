#include <iostream>
#include "form.hpp"
#include "blinker.hpp"
#include "block.hpp"
#include "glider.hpp"
#include "menu.hpp"

void initConfig(int *line, int *column, int *iterations, int *size, Menu *menu);
void newBlock(int line, int column, int iterations, int size);
void newBlinker(int line, int column, int iterations, int size);
void newGlider(int line, int column, int iterations, int size);
void newGosper(int line, int column, int iterations, int size);

// MAIN FUNCTION

int main(int argc, char const *argv[])
{
	int line, column, iterations, size;   
	Menu menu;

	initConfig(&line, &column, &iterations, &size, &menu);

	if(menu.getOption()==1)
		newBlock(line, column, iterations, size);
	if(menu.getOption()==2) 
		newBlinker(line, column, iterations, size);
	if(menu.getOption()==3)
		newGlider(line, column, iterations, size);
	if(menu.getOption()==4)
		newGosper(line, column, iterations, size);

	return 0;
}


// FUNCTIONS

void initConfig(int *line, int *column, int *iterations, int *size, Menu *menu) {
	*line = menu->getLine();
	*column = menu->getColumn();
	*iterations = menu->getIterations();
	*size = menu->getSize();
}

void newBlock(int line, int column, int iterations, int size) {
	Block foal(line, column);
	foal.setIterations(iterations);
	foal.setSize(size);
	foal.printMatriz();
}

void newBlinker(int line, int column, int iterations, int size) {
	Blinker foal(line, column);
	foal.setIterations(iterations);
	foal.setSize(size);
	foal.printMatriz();
}

void newGlider(int line, int column, int iterations, int size) {
	Glider foal(line, column);
	foal.setIterations(iterations);
	foal.setSize(size);
	foal.printMatriz();
}

void newGosper(int line, int column, int iterations, int size) {
	Form field;
	field.newMatriz();
	Block foal1(4, 0, &field);
	Block foal2(2, 34, &field);
	Blinker foal3(5, 10, &field);
	Blinker foal4(5, 16, &field);
	Blinker foal5(3, 20, &field);
	Blinker foal6(3, 21, &field);
	field.setMatrizPoint('o', 3, 11);
	field.setMatrizPoint('o', 7, 11);
	field.setMatrizPoint('o', 2, 12);
	field.setMatrizPoint('o', 8, 12);
	field.setMatrizPoint('o', 2, 13);
	field.setMatrizPoint('o', 8, 13);
	field.setMatrizPoint('o', 5, 14);
	field.setMatrizPoint('o', 3, 15);
	field.setMatrizPoint('o', 7, 15);
	field.setMatrizPoint('o', 5, 17);
	field.setMatrizPoint('o', 1, 22);
	field.setMatrizPoint('o', 5, 22);
	field.setMatrizPoint('o', 0, 24);
	field.setMatrizPoint('o', 1, 24);
	field.setMatrizPoint('o', 5, 24);
	field.setMatrizPoint('o', 6, 24);
	field.setIterations(iterations);
	field.printMatriz();
}