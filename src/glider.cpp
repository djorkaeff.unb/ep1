#include "glider.hpp"

Glider::Glider() {
	setMatrizPoint('o', 5, 5);
	setMatrizPoint('o', 6, 6);
	setMatrizPoint('o', 6, 7);
	setMatrizPoint('o', 5, 7);
	setMatrizPoint('o', 4, 7);
}

Glider::Glider(int line, int column, Form *p) {
	p->setMatrizPoint('o', line+1, column+1); 
	p->setMatrizPoint('o', line+2, column+2);
	p->setMatrizPoint('o', line+2, column+3);
	p->setMatrizPoint('o', line+1, column+3);
	p->setMatrizPoint('o', line, column+3);
}

Glider::Glider(int line, int column) {
	newMatriz();
	setMatrizPoint('o', line+1, column+1); 
	setMatrizPoint('o', line+2, column+2);
	setMatrizPoint('o', line+2, column+3);
	setMatrizPoint('o', line+1, column+3);
	setMatrizPoint('o', line, column+3);
}