#include "menu.hpp"
#include <iostream>
#include <stdio.h>
#include <ctype.h>

Menu::Menu() {
	int option, line, column, iterations, size;
	char switchChar;

	std::cout<< "\033[2J";
	std::cout<< "\033[0;0f";
    std::cout << std::endl;
    std::cout << " >> CONWAY'S GAME OF LIFE <<" << std::endl;
    std::cout << " 1 - BLOCK" << std::endl;
    std::cout << " 2 - BLINKER" << std::endl;
    std::cout << " 3 - GLIDER" << std::endl;
    std::cout << " 4 - GOSPER GLIDER GUN" << std::endl;
    std::cout << " Opção: ";
    std::cin >> option;
    setOption(option);
    std::cout<< "\033[2J";
	std::cout<< "\033[0;0f";
    if(getOption()!=4) {
    	std::cout << "Posição inicial padrão [1][1]" << std::endl;
		std::cout << "Deseja alterar a posição inicial? ('S' para sim ou qualquer tecla para não..)" << std::endl;
		std::cin >> switchChar;
		switchChar = toupper(switchChar);
		setOpPos(switchChar);
		std::cout << "Matriz padrão 36x36" << std::endl;
		std::cout << "Deseja alterar o tamanho da matriz? ('S' para sim ou qualquer tecla para não..)" << std::endl;
		std::cin >> switchChar;
		switchChar = toupper(switchChar);
		setOpSize(switchChar);
	}
	std::cout << "Número de iterações padrão 10" << std::endl;
	std::cout << "Deseja alterar o número de iterações? ('S' para sim ou qualquer tecla para não..)" << std::endl;
	std::cin >> switchChar;
	switchChar = toupper(switchChar);
	setOpIter(switchChar);

	setLine(1);
	setColumn(1);
	setIterations(10);
	setSize(36);

	if(getOpPos()=='S' && getOption()!=4) {
		std::cout<< "\033[2J";
		std::cout<< "\033[0;0f";
		do{
			std::cout << "Digite a posição desejada: (linha coluna) [Devem ser maiores que 0]" << std::endl;
			std::cin >> line;
			std::cin >> column;
			if(line<1 || column<1) {
				std::cout<< "\033[2J";
				std::cout<< "\033[0;0f";
				std::cout << "Linha e coluna devem ser maiores que 0!" << std::endl;
			}
		} while(line<1 || column<1);
		setLine(line);
		setColumn(column);
	}

	if(getOpSize()=='S' && getOption()!=4) {
		std::cout<< "\033[2J";
		std::cout<< "\033[0;0f";
		do {		
			std::cout << "Digite o tamanho de matriz desejado: (N x N) [Digite somente N]" << std::endl;
			std::cin >> size;
			if(size>36) {
				std::cout<< "\033[2J";
				std::cout<< "\033[0;0f";
				std::cout << "O tamanho não deve ser maior que 36!" << std::endl;
			}
		} while(size>36);
		setSize(size);
	}

	if(getOpIter()=='S') {
		std::cout<< "\033[2J";
		std::cout<< "\033[0;0f";
		do {
			std::cout << "Digite o número de iterações desejado: " << std::endl;
			std::cin >> iterations;
			if(iterations<1) {
				std::cout<< "\033[2J";
				std::cout<< "\033[0;0f";
				std::cout << "O número de iterações deve ser maior que 0!" << std::endl;
			}
		} while(iterations<1);
		setIterations(iterations);
	}
}

void Menu::setOption(int option) {
	this->option = option;
}

int Menu::getOption() {
	return option;
}

void Menu::setOpPos(char switchChar) {
	opPosition = switchChar;
}

void Menu::setOpIter(char switchChar) {
	opIteration = switchChar;
}

void Menu::setOpSize(char switchChar) {
	opSize = switchChar;
}

char Menu::getOpPos() {
	return opPosition;
}

char Menu::getOpIter() {
	return opIteration;
}

char Menu::getOpSize() {
	return opSize;
}

void Menu::setLine(int line) {
	this->line = line;
}

void Menu::setColumn(int column) {
	this->column = column;
}

void Menu::setIterations(int iterations) {
	this->iterations = iterations;
}

void Menu::setSize(int size) {
	this->size = size;
}

int Menu::getLine() {
	return line;
} 

int Menu::getColumn() {
	return column;
}

int Menu::getIterations() {
	return iterations;
}

int Menu::getSize() {
	return size;
}