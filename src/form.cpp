#include <iostream>
#include <stdio.h>
#include "form.hpp"
#include <time.h> // Para a struct timespec

int nsleep(long miliseconds);

using namespace std;

Form::Form(){
    setIterations(10);
    setSize(36);
}

void Form::newMatriz() {
    // Preenche todos os espaços com vazio
    for(int i=0; i<MAX; i++) {
        for(int j=0; j<MAX; j++) {
            matriz[i][j] = ' ';
        }
    }
}

int Form::getSize() {
    return size;
}

void Form::setSize(int size) {
    this->size = size;
}

char Form::getMatrizPoint(int line, int column) {
    return matriz[line][column];
}

void Form::setMatrizPoint(char Status, int line, int column) {
    matriz[line][column] = Status;
}

int Form::getIterations() {
    return iterations;
}
 
void Form::setIterations(int iterations) {
    this->iterations = iterations;
}

void Form::printMatriz() {
       
    for(int k=0; k<getIterations(); k++) {
        std::cout<< "\033[2J";
        std::cout<< "\033[0;0f";
        for(int z=0; z<getSize()-5; z++) {
            printf(" ");
        }
        printf("* - - - - - - *\n");
        for(int z=0; z<getSize()-5; z++) {
            printf(" ");
        }
        printf("| Geração %2.d  |\n", k+1);
        for(int z=0; z<getSize()-5; z++) {
            printf(" ");
        }
        printf("* - - - - - - *\n");
        printf("* ");
        for(int z=2; z<=getSize(); z++) {
            printf("- ");
        }
        printf("*");
        printf("\n");
        for(int i=0; i<getSize(); i++) {
            printf("|");
            for(int j=0; j<getSize(); j++) {
                printf("%c", getMatrizPoint(i, j));
                if(j!=getSize()-1) {
                    printf(" ");
                }
            }
            printf("|\n");
        }
        printf("* ");
        for(int z=2; z<=getSize(); z++) {
            printf("- ");
        }
        printf("*");
        printf("\n");
        setRules();
        nsleep(100);
    }
    
}

int Form::getNeighbors(int line, int column) {

    return neighbors[line][column];
}

void Form::setNeighbors() {

    int nNeighbors;

    for(int i=0; i<getSize(); i++) {
        for(int j=0; j<getSize(); j++) {
            nNeighbors=0;
            if(getMatrizPoint(i-1, j-1)=='o')
                nNeighbors++;
            if(getMatrizPoint(i+1, j+1)=='o')
                nNeighbors++;
            if(getMatrizPoint(i-1, j)=='o')
                nNeighbors++;
            if(getMatrizPoint(i-1, j+1)=='o')
                nNeighbors++;
            if(getMatrizPoint(i, j-1)=='o')
                nNeighbors++;
            if(getMatrizPoint(i, j+1)=='o')
                nNeighbors++;
            if(getMatrizPoint(i+1, j-1)=='o')
                nNeighbors++;
            if(getMatrizPoint(i+1, j)=='o')
                nNeighbors++;

            neighbors[i][j] = nNeighbors;
        }
    }
}

void Form::setRules() {

    setNeighbors();

    for(int i=0; i<getSize(); i++) {
        for(int j=0; j<getSize(); j++) {
            if(getMatrizPoint(i, j)=='o') {
                if(getNeighbors(i, j)<2)
                    setMatrizPoint(' ', i, j);
                if(getNeighbors(i, j)>3)
                    setMatrizPoint(' ', i, j);
            }

            // Se a celula estive morta
            if(getMatrizPoint(i, j)==' ' && getNeighbors(i, j)==3) {
                setMatrizPoint('o', i, j);
            }

            if(getMatrizPoint(i, j)=='o' && (getNeighbors(i, j)==3 || getNeighbors(i, j)==2)) {
                setMatrizPoint('o', i, j);
            }
        }
    }

}

int nsleep(long miliseconds)
{
   struct timespec req, rem;

   if(miliseconds > 999)
   {   
        req.tv_sec = (int)(miliseconds / 1000);                            
        req.tv_nsec = (miliseconds - ((long)req.tv_sec * 1000)) * 1000000; 
   }   
   else
   {   
        req.tv_sec = 0;                        
        req.tv_nsec = miliseconds * 1000000;
   }   

   return nanosleep(&req , &rem);
}
