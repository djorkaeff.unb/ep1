#include "block.hpp"

Block::Block() {
	setMatrizPoint('o', 2, 2);
	setMatrizPoint('o', 2, 3);
	setMatrizPoint('o', 3, 2);
	setMatrizPoint('o', 3, 3);
}

Block::Block(int line, int column, Form *p) {
	p->setMatrizPoint('o', line, column);
	p->setMatrizPoint('o', line, column+1);
	p->setMatrizPoint('o', line+1, column);
	p->setMatrizPoint('o', line+1, column+1);
}

Block::Block(int line, int column) {
	newMatriz();
	setMatrizPoint('o', line, column);
	setMatrizPoint('o', line, column+1);
	setMatrizPoint('o', line+1, column);
	setMatrizPoint('o', line+1, column+1);
}