#include "blinker.hpp"

Blinker::Blinker() {
	setMatrizPoint('o', 5, 4);
	setMatrizPoint('o', 5, 5);
	setMatrizPoint('o', 5, 6);
}

Blinker::Blinker(int line, int column, Form *p) {
	p->setMatrizPoint('o', line, column);
	p->setMatrizPoint('o', line+1, column);
	p->setMatrizPoint('o', line-1, column);
}

Blinker::Blinker(int line, int column) {
	newMatriz();
	setMatrizPoint('o', line, column);
	setMatrizPoint('o', line+1, column);
	setMatrizPoint('o', line-1, column);
}