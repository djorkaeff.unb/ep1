#ifndef FORM_HPP
#define FORM_HPP

#define MAX 37

class Form {
	private:	
		char matriz[MAX][MAX];
		int iterations, size;
		int neighbors[MAX][MAX];

	public:
		Form();
		void newMatriz();
		void setSize(int size);		
		int getSize();
		char getMatrizPoint(int line, int column);
		void setMatrizPoint(char Status, int line, int column);
		int getNeighbors(int line, int column);
		void setNeighbors();
		void setRules();
		void setIterations(int iterations);
		int getIterations();
		void printMatriz();
};

#endif

