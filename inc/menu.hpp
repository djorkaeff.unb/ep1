#ifndef MENU_HPP
#define MENU_HPP

class Menu {
	private:
		int option, line, column, iterations, size;
		char opIteration, opPosition, opSize;
	public:
		Menu();
		void setOption(int option);
		int getOption();
		void setOpPos(char op);
		char getOpPos();
		void setOpIter(char op);
		char getOpIter();
		void setOpSize(char op);
		char getOpSize();
		void setLine(int line);
		void setColumn(int column);
		void setIterations(int iterations); 
		void setSize(int size); 
		int getLine();
		int getColumn();
		int getIterations(); 
		int getSize();
};
#endif