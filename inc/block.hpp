#ifndef BLOCK_HPP
#define BLOCK_HPP
#include "form.hpp"

class Block : public Form {
	public:
		Block();
		Block(int line, int column, Form *p);
		Block(int line, int column);
};

#endif