#ifndef BLINKER_HPP
#define BLINKER_HPP
#include "form.hpp"

class Blinker : public Form {
	private:

	public:
		Blinker();
		Blinker(int line, int column, Form *p);
		Blinker(int line, int column);
};

#endif