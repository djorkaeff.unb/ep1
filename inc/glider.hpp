#ifndef GLIDER_HPP
#define GLIDER_HPP
#include "form.hpp"

class Glider : public Form {
	private:

	public:
		Glider();
		Glider(int line, int column, Form *p);
		Glider(int line, int column);
};

#endif