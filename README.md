# EP1 - OO 2017/02

Djorkaeff Alexandre Vilela Pereira - 16/0026822

Jogo da matéria Orientação a Objetos

O projeto de Orientação consiste em uma versão do jogo __"Conway's Game of Life"__, com a intenção de aprender mais sobre a linguagem C++ e aplicar o paradigma de Orientação a Objetos com o uso de herança e polimorfismo.

# Makefile

Makefile do projeto tem 3 opções simples:

__"make"__ : compila as pastas do jogo e envia os objetos para obj/ e os binários para bin/

__"make clean"__ : limpa as pastas obj/ e bin/

__"make run"__: inicia o jogo no terminal

# Jogo

O jogo funciona da seguinte maneira, deve ser escolhida inicialmente uma forma que consiste em células mortas ou vivas que será representada no display tendo como __'o'__ as células vivas e as células mortas são representadas somente por __' ' (space)__.
Em seguida o jogador pode optar por mudar a posição inicial da forma, o tamanho da matriz na qual a forma será inserida e também
o número de iterações que o campo irá sofrer (número de vezes que as __regras de vida__ serão aplicadas as células), usando a tecla *'S'*
para concordar com alterações nas configurações padrões de campo.
Após a seleção das configuções de campo disponíveis para cada forma a forma será representada numa matriz e será iterada conforme as
__regras de vida__.

Para mais informações sobre a idéia do jogo: [Conway's Game of Life](https://pt.wikipedia.org/wiki/Jogo_da_vida).
